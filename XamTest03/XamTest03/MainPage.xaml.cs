﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamTest03
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<string> contactList;

        public MainPage(string username)
        {
            InitializeComponent();

            LabelMessage.Text = "Hi " + username + " , create your contact list";

            contactList = new ObservableCollection<string>();
            ListViewContactList.ItemsSource = contactList;

        }

        private void ButtonAdd_Clicked(object sender, EventArgs e)
        {
            LabelMessage.Text = "";
            if (string.IsNullOrEmpty(EntryName.Text))
            {
                LabelMessage.Text = "Contact name is empty";
                return;
            }

            contactList.Add(EntryName.Text);
            LabelMessage.Text = EntryName.Text + " added";
            EntryName.Text = "";
        }
    }
}
