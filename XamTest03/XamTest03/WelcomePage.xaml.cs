﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace XamTest03
{
    public partial class WelcomePage : ContentPage
    {
        public WelcomePage()
        {
            InitializeComponent();
        }

        public void buttonGo_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryUsername.Text))
            {
                labelMessage.Text = "username is empty";
                entryUsername.Focus();
                return;
            }

            Navigation.PushAsync(new MainPage(entryUsername.Text));
        }
    }
}
